# Medilabo_Master

Comment lancer le projet:

1/ Clonez le repository Medilabo_Master dans un dossier

2/ Dans le repository Medilabo_Master executez la commande
`git submodule update --init --recursive`
Afin de cloner les sous modules

3/ Assurez vous que le daemon docker tourne correctement sur votre machine et executez
`docker compose up`
Attendez que toutes les images soient correctement montées, et que les containers: - medilabo_authentservice - medilabo_diabeteservice - medilabo_frontend - medilabo_gateway - medilabo_notesservice - medilabo_patientservice - medilabo-mongodb - medilabo-postgresdb

tournent correctement.

accedez à http://localhost pour commencer à utiliser l'application.

# Pistes d'améliorations Green Code

1/ Réduire le nombre d'appels au PatientService et au NoteService lors du calcul du diabète.
2/ Optimiser le code Frontend ( GreenIT Analyzer )
